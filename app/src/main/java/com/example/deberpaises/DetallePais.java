package com.example.deberpaises;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetallePais extends AppCompatActivity {

    ImageView imageView;
    TextView textView;
    WebView webView;

    private String link;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_pais);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imageView = (ImageView)findViewById(R.id.imagen);
        textView =(TextView)findViewById(R.id.texto);
        webView =(WebView) findViewById(R.id.video);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());

        //imageView.setImageResource(getIntent().getExtras().getInt("curImagen"));
        Picasso.with(imageView.getContext())
                .load(getIntent().getExtras().getString("curImagen")).into(imageView);
        textView.setText(getIntent().getExtras().getString("curNombre"));
       webView.loadUrl(getIntent().getExtras().getString("curVideo"));


    }


}
