package com.example.deberpaises;

import java.io.Serializable;

public class Modelo implements Serializable {
    private String imagen;
    private String nombre;
    private String video;


    public Modelo() {
    }

    public Modelo(String imagen, String nombre, String video) {
        this.imagen = imagen;
        this.nombre = nombre;
        this.video = video;

    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }


}
