package com.example.deberpaises;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class GridViewAdapter extends RecyclerView.Adapter<GridViewAdapter.GridViewHolder>{
private List<Modelo>items;

    @NonNull
    @Override
    public GridViewAdapter.GridViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v= LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.pais_card,viewGroup,false);

        return new GridViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull GridViewAdapter.GridViewHolder gridViewHolder, final int i) {

       // gridViewHolder.imageView.setImageResource(items.get(i).getImagen());

        Picasso.with(gridViewHolder.imageView.getContext())
                .load(items.get(i).getImagen()).into(gridViewHolder.imageView);


        gridViewHolder.textView.setText(items.get(i).getNombre());


        gridViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("curImagen",items.get(i).getImagen());
                bundle.putString("curNombre",items.get(i).getNombre());
                bundle.putString("curVideo",items.get(i).getVideo());
                Intent intent =new Intent(v.getContext(),DetallePais.class);
                intent.putExtras(bundle);
                v.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public GridViewAdapter(List<Modelo>items){
        this.items=items;
    }

    public List<Modelo>getItems(){
        return this.items;
    }

    public class GridViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;
        public ImageView imageView;
        public TextView textView;
        public WebView webView;

        public GridViewHolder (View view){
            super(view);
             cardView=(CardView)view.findViewById(R.id.card);
             imageView=(ImageView)view.findViewById(R.id.imagen);
             textView=(TextView)view.findViewById(R.id.texto);
             webView=(WebView)view.findViewById(R.id.video);
        }
    }
}
