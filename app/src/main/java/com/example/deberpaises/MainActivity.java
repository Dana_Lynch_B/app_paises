package com.example.deberpaises;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

   private List<Modelo>items=new ArrayList<>();
   private RecyclerView recyclerView;
   private RecyclerView.Adapter adapter;
   private RecyclerView.LayoutManager layoutManager;

    //objeto de conexion a firestore
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = FirebaseFirestore.getInstance();

        Pais();

        subirCloudFirestore();

        //obtener recyclerView
        recyclerView=(RecyclerView)findViewById(R.id.recyclador);
        recyclerView.setHasFixedSize(true);

        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        db.collection("pais")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Modelo> paises = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Map<String,Object> md = document.getData();
                                String imagen = (String) md.get("imagen");
                                String nombre = (String) md.get("nombre");
                                String video = (String)md.get("video");
                                paises.add(new Modelo(imagen ,nombre,video));
                            }

                            adapter= new GridViewAdapter(paises);
                            recyclerView.setAdapter(adapter);

                        }
                    }
                });



    }


    private void Pais(){
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/afganistan-bandera-200px.jpg","Afganistán" +
                "                                                                                                               Población: 38.928.341 habitantes","https://www.youtube.com/watch?v=dw5f5w3N_Ks"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/albania-bandera-200px.jpg","Albania      " +
                "                                                                                                               Población: 2.877.800 habitantes","https://www.youtube.com/watch?v=ynJVnloEogA"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/alemania-bandera-200px.jpg"," Alemania      " +
                "                                                                                                               Población: 83.783.945 habitantes","https://www.youtube.com/watch?v=rJWUseVRlk4"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/andorra-bandera-200px.jpg","Andorra        " +
                "                                                                                                               Población: 77.506 habitantes","https://www.youtube.com/watch?v=32ixbsBo1nk"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/angola-bandera-200px.jpg","Angola     " +
                "                                                                                                               Población: 32.866.268 habitantes","https://www.youtube.com/watch?v=MP8GNFX2VH0"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/antigua-y-barbuda-bandera-200px.jpg","Antigua y Barbuda  " +
                "                                                                                                               Población: 96.286 habitantes","https://www.youtube.com/watch?v=-n8amVqdTlg"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/arabia-saudita-bandera-200px.jpg","Arabia Saudita " +
                "                                                                                                               Población: 33.699.947 habitantes","https://www.youtube.com/watch?v=p57H2I3Ojo0"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/argelia-bandera-200px.jpg","Argelia " +
                "                                                                                                               Población: 42.228.429 habitantes","https://www.youtube.com/watch?v=dDOjVGIwHIA"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/argentina-bandera-200px.jpg","Argentina " +
                "                                                                                                               Población: 45,3 habitantes","https://www.youtube.com/watch?v=OqSQo2aifAA"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/armenia-bandera-200px.jpg","Armenia " +
                "                                                                                                               Población: 3.073.543 habitantes","https://www.youtube.com/watch?v=XAkO488S-Lg"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/australia-bandera-200px.jpg","Australia " +
                "                                                                                                               Población: 25.169.000 habitantes","https://www.youtube.com/watch?v=xnfcB3iv7xI"));

        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/austria-bandera-200px.jpg","Austria " +
                "                                                                                                               Población: 8.901.064 habitantes","https://www.youtube.com/watch?v=0pAZM2hgWkY"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/azerbaiyan-bandera-200px.jpg","Azerbaiyán " +
                "                                                                                                               Población: 10.067.108 habitantes","https://www.youtube.com/watch?v=SLlI8hM6btw"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/bahamas-bandera-200px.jpg","Bahamas " +
                "                                                                                                               Población: 377.000 habitantes","https://www.youtube.com/watch?v=T07bEFAU0YA"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/banglades-bandera-200px.jpg","Bangladés " +
                "                                                                                                               Población: 1.115.62 habitantes","https://www.youtube.com/watch?v=fZFNgAbLLIk"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/barbados-bandera-200px.jpg","Barbados " +
                "                                                                                                               Población: 286.000 habitantes","https://www.youtube.com/watch?v=KStk0fJ9G48"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/barein-bandera-200px.jpg","Baréin " +
                "                                                                                                               Población: 1.569.439 habitantes","https://www.youtube.com/watch?v=WL4WUiTlfH8"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/belgica-bandera-200px.jpg","Bélgica " +
                "                                                                                                               Población: 11.549.888 habitantes","https://www.youtube.com/watch?v=jFA-PdqfbiM"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/belice-bandera-200px.jpg","Belice " +
                "                                                                                                               Población: 383.071 habitantes","https://www.youtube.com/watch?v=TQPUqb0QSr4"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/benin-bandera-200px.jpg","Benín " +
                "                                                                                                               Población: 12.450.867 habitantes","https://www.youtube.com/watch?v=JDzjadRDpNc"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/bielorrusia-bandera-200px.jpg","Bielorrusia " +
                "                                                                                                               Población: 9.408.350 habitantes","https://www.youtube.com/watch?v=QOBvAAMHOy0"));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/birmania-myanmar-bandera-200px.jpg","Birmania",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/bolivia-bandera-200px.jpg","Bolivia",""));

        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/bosniza-y-herzegovina-bandera-200px.jpg","Bosnia y Herzegovina",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/botsuana-bandera-200px.jpg","Botsuana",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/brasil-bandera-200px.jpg","Brasil",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/brunei-bandera-200px.jpg","Brunéi",""));
        items.add(new Modelo("https://www.comprarbanderas.es/images/banderas/400/35-bulgaria-ce_400px.jpg","Bulgaria",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/burkina-faso-bandera-200px.jpg","Burkina Faso",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/burundi-bandera-200px.jpg","Burundi",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/butan-bandera-200px.jpg","Bután",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/cabo-verde-bandera-200px.jpg","Cabo Verde",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/camboya-bandera-200px.jpg","Camboya",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/camerun-bandera-200px.jpg","Camerún",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/canada-bandera-200px.jpg","Canadá",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/catar-bandera-200px.jpg","Catar",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/chad-bandera-200px.jpg","Chad",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/chile-bandera-200px.jpg","Chile",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/china-bandera-200px.jpg","China",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/chipre-bandera-200px.jpg","Chipre",""));

        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/vaticano-bandera-200px.jpg","Ciudad del Vaticano",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/colombia-bandera-200px.jpg","Colombia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/comoras-bandera-200px.jpg","Comoras",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/corea-norte-bandera-200px.jpg","Corea del Norte",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/corea-sur-bandera-200px.jpg","Corea del Sur",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/costa-de-marfil-bandera-200px.jpg","Costa de Marfil",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/costa-rica-bandera-200px.jpg","Costa Rica",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/croacia-bandera-200px.jpg","Croacia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/cuba-bandera-200px.jpg","Cuba",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/dinamarca-bandera-200px.jpg","Dinamarca",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/dominica-bandera-200px.jpg","Dominica",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/ecuador-bandera-200px.jpg","Ecuador",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/egipto-bandera-200px.jpg","Egipto",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/el-salvador-bandera-200px.jpg","El Salvador",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/emiratos-arabes-unidos-bandera-200px.jpg","Emiratos Árabes Unidos",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/eritrea-bandera-200px.jpg","Eritrea",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/eslovaquia-bandera-200px.jpg","Eslovaquia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/eslovenia-bandera-200px.jpg","Eslovenia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/espana-bandera-200px.jpg","España",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/eeuu-bandera-200px.jpg","Estados Unidos",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/estonia-bandera-200px.jpg","Estonia",""));

        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/etiopia-bandera-200px.jpg","Etiopía",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/filipinas-bandera-200px.jpg","Filipinas",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/finlandia-banera-200px.jpg","Finlandia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/fiyi-bandera-200px.jpg","Fiyi",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/francia-bandera-200px.jpg","Francia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/gabon-bandera-200px.jpg","Gabón",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/gambia-bandera-200px.jpg","Gambia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/georgia-bandera-200px.jpg","Georgia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/ghana-bandera-200px.jpg","Ghana",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/granada-pais-bandera-200px.jpg","Granada",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/grecia-bandera-200px.jpg","Grecia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/guatemala-bandera-200px.jpg","Guatemala",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/guyana-bandera-200px.jpg","Guyana",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/guinea-bandera-200px.jpg","Guinea",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/guinea-ecuatorial-bandera-200px.jpg","Guinea ecuatorial",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/guinea-bisau-bandera-200px.jpg","Guinea-Bisáu",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/haiti-bandera-200px.jpg","Haití",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/honduras-bandera-200px.jpg","Honduras",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/hungria-bandera-200px.jpg","Hungría",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/india-bandera-200px.jpg","India",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/indonesia-bandera-200px.jpg","Indonesia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/irak-bandera-200px.jpg","Irak",""));

        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/iran-bandera-200px.jpg","Irán",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/irlanda-bandera-200px.jpg","Irlanda",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/islandia-bandera-200px.jpg","Islandia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/islas-marshall-bandera-200px.jpg","Islas Marshall",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/islas-salomon-bandera-200px.jpg","Islas Salomón",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/israel-bandera-200px.jpg","Israel",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/italia-bandera-200px.jpg","Italia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/jamaica-bandera-200px.jpg","Jamaica",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/japon-bandera-200px.jpg","Japón",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/jordania-bandera-200px.jpg","Jordania",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/kazajistan-bandera-200px.jpg","Kazajistán",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/kenia-bandera-200px.jpg","Kenia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/kirguistan-bandera-200px.jpg","Kirguistán",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/kiribati-bandera-200px.jpg","Kiribati",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/kuwait-bandera-200px.jpg","Kuwait",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/laos-bandera-200px.jpg","Laos",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/lesoto-bandera-200px.jpg","Lesoto",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/letonia-bandera-200px.jpg","Letonia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/libano-bandera-200px.jpg","Líbano",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/liberia-bandera-200px.jpg","Liberia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/libia-bandera-200px.jpg","Libia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/liechtenstein-bandera-200px.jpg","Liechtenstein",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/lituania-bandera-200px.jpg","Lituania",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/luxemburgo-bandera-200px.jpg","Luxemburgo",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/macedonia-bandera-200px.jpg","Macedonia del Norte",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/madagascar-bandera-200px.jpg","Madagascar",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/malasia-bandera-200px.jpg","Malasia",""));

        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/malaui-bandera-200px.jpg","Malaui",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/maldivas-bandera-200px.jpg","Maldivas",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/mali-bandera-200px.jpg","Malí",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/malta-bandera-200px.jpg","Malta",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/marruecos-bandera-200px.jpg","Marruecos",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/mauricio-bandera-200px.jpg","Mauricio",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/mauritania-bandera-actualizada-200px.jpg","Mauritania",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/mexico-bandera-200px.jpg","México",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/micronesia-bandera-200px.jpg","Micronesia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/moldavia-bandera-200px.jpg","Moldavia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/monaco-bandera-200px.jpg","Mónaco",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/mongolia-bandera-200px.jpg","Mongolia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/montenegro-bandera-200px.jpg","Montenegro",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/mozambique-bandera-200px.jpg","Mozambique",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/namibia-bandera-200px.jpg","Namibia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/nauru-bandera-200px.jpg","Nauru",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/nepal-bandera-200px.jpg","Nepal",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/nicaragua-bandera-200px.jpg","Nicaragua",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/niger-bandera-200px.jpg","Níger",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/nigeria-bandera-200px.jpg","Nigeria",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/noruega-bandera-200px.jpg","Noruega",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/nueva-zelanda-bandera-200px.jpg","Nueva Zelanda",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/oman-bandera-200px.jpg","Omán",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/paises-bajos-bandera-200px.jpg","Países Bajos",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/pakistan-bandera-200px.jpg\n","Pakistán",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/palaos-bandera-200px.jpg\n","Palaos",""));

        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/panama-bandera-200px.jpg","Panamá",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/papua-nueva-guinea-bandera-200px.jpg","Papúa Nueva Guinea",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/paraguay-bandera-200px.jpg","Paraguay",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/peru-bandera-200px.jpg","Perú",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/polonia-bandera-200px.jpg","Polonia",""));
        items.add(new Modelo("hhttps://www.saberespractico.com/wp-content/uploads/2015/06/portugal-bandera-200px.jpg","Portugal",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/reino-unido-bandera-200px.jpg","Reino Unido",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/republica-centroafricana-bandera-200px.jpg","República Centroafricana",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/republica-checa-bandera-200px.jpg","República Checa",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/republica-del-congo-bandera-200px.jpg","República del Congo",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/republica-democratica-del-congo-bandera-200px.jpg","República Democrática del Congo",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/republica-dominicana-bandera-200px.jpg","República Dominicana",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/sudafrica-bandera-200px.jpg","República Sudafricana",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/ruanda-bandera-200px.jpg","Ruanda",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/rumania-bandera-200px.jpg","Rumanía",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/rusia-bandera-200px.jpg","Rusia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/samoa-bandera-200px.jpg","Samoa",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/san-cristobal-y-nieves-bandera-200px.jpg","San Cristóbal y Nieves",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/san-marino-bandera-200px.jpg","San Marino",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/san-vicente-y-las-granadinas-bandera-200px.jpg","San Vicente y las Granadinas",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/santa-lucia-bandera-200px.jpg","Santa Lucía",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/santo-tome-y-principe-bandera-200px.jpg","Santo Tomé y Príncipe",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/senegal-bandera-200px.jpg","Senegal",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/serbia-bandera-200px.jpg","Serbia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/seychelles-bandera-200px.jpg","Seychelles",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/sierra-leona-bandera-200px.jpg","Sierra Leona",""));

        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/singapur-bandera-200px.jpg","Singapur",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/siria-bandera-200px.jpg","Siria",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/somalia-bandera-200px.jpg","Somalia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/sri-lanka-bandera-200px.jpg","Sri Lanka",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/suazilandia-bandera-200px.jpg","Suazilandia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/sudan-bandera-200px.jpg","Sudán",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/sudan-del-sur-bandera-200px.jpg","Sudán del Sur",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/suecia-bandera-200px.jpg","Suecia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/suiza-bandera-200px.jpg","Suiza",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/surinam-bandera-200px.jpg","Surinam",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/tailandia-bandera-200px.jpg","Tailandia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/tanzania-bandera-200px.jpg","Tanzania",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/tayikistan-bandera-200px.jpg","Tayikistán",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/timor-oriental-bandera-200px.jpg","Timor Oriental",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/togo-bandera-200px.jpg","Togo",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/tonga-bandera-200px.jpg","Tonga",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/trinidad-y-tobago-bandera-200px.jpg","Trinidad y Tobago",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/tunez-bandera-200px.jpg","Túnez",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/turkmenistan-bandera-200px.jpg","Turkmenistán",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/turquia-bandera-200px.jpg","Turquía",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/tuvalu-bandera-200px.jpg","Tuvalu",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/ucrania-bandera-200px.jpg","Ucrania",""));

        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/uganda-bandera-200px.jpg","Uganda",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/uruguay-bandera-200px.jpg","Uruguay",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/uzbekistan-bandera-200px.jpg","Uzbekistán",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/vanuatu-bandera-200px.jpg","Vanuatu",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/04/venezuela-bandera-200px.jpg","Venezuela",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/vietnam-bandera-200px.jpg","Vietnam",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/06/yemen-bandera-200px.jpg","Yemen",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/yibuti-bandera-200px.jpg","Yibuti",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/zambia-bandera-200px.jpg","Zambia",""));
        items.add(new Modelo("https://www.saberespractico.com/wp-content/uploads/2015/07/zimbabue-bandera-200px.jpg","Zimbabue",""));



    }


    private void subirCloudFirestore(){
        for (Modelo p : items) {
            Map<String,Object> mapa = new HashMap<>();
            mapa.put("imagen",p.getImagen());
            mapa.put("nombre",p.getNombre());
            mapa.put("video",p.getVideo());
            db.collection("pais")
                    .add(mapa)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Log.d("FB","Pais agregado correctamente");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("FB","Pais no agregado",e);
                        }
                    });
        }

}
}
   /*
   Afganistán
    Albania
    Alemania
    Andorra
    Angola
    Antigua y Barbuda
    Arabia Saudita








   public void CargarDatos() throws IOException {

        List<String> listado = new ArrayList<>();
        String nombre;

        InputStream is = this.getResources().openRawResource(R.raw.paises);
        BufferedReader leer = new BufferedReader(new InputStreamReader(is));
        if(is!=null){
            while ((nombre = leer.readLine())!=null){
                listado.add(nombre.split(" ")[0]);

            }
        }
        is.close();

        String datos[] = listado.toArray(new String[listado.size()]);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,datos);
        Toast.makeText(this, "Total Paises: "+listado.size(), Toast.LENGTH_SHORT).show();
        grid.setAdapter(adapter);
    }
}*/

/*
    private GridViewAdapter gridViewAdapter;
    private GridView gridView;
    String[] items ={
            /*"https://i.pinimg.com/originals/0f/90/ce/0f90cee4ba801d16b02fceb11909ed21.jpg",
             "https://i.pinimg.com/originals/0f/90/ce/0f90cee4ba801d16b02fceb11909ed21.jpg",
             "https://i.pinimg.com/originals/0f/90/ce/0f90cee4ba801d16b02fceb11909ed21.jpg",
             "https://i.pinimg.com/originals/0f/90/ce/0f90cee4ba801d16b02fceb11909ed21.jpg",
             "https://i.pinimg.com/originals/0f/90/ce/0f90cee4ba801d16b02fceb11909ed21.jpg",
             "https://i.pinimg.com/originals/0f/90/ce/0f90cee4ba801d16b02fceb11909ed21.jpg",
             "https://i.pinimg.com/originals/0f/90/ce/0f90cee4ba801d16b02fceb11909ed21.jpg",
             "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSlErwaJ0F2-ykUtIkmsSsFQTBn9Ysiu4GMsbXXahsoPjyXfJ5T&s",



    };*/

/*gridView =(GridView) findViewById(R.id.grid);
        gridViewAdapter= new GridViewAdapter(MainActivity.this,items);

        gridView.setAdapter(gridViewAdapter);*/